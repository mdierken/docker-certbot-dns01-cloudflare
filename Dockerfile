FROM certbot/certbot
COPY certbot/certbot-dns-cloudflare src/certbot-dns-cloudflare
RUN pip install --no-cache-dir --editable src/certbot-dns-cloudflare