#!/bin/bash
source .env
docker-compose build
docker run --rm -v $AUTH_DIR/credentials.ini:/root/cloudflare.ini:ro \
    -v $SSL_DIR/$DOMAIN:/etc/letsencrypt \
    \
    $REGISTRY/certbot-cloudflare certonly \
    --email $REGISTRATION_EMAIL \
    --agree-tos \
    --dns-cloudflare \
    --dns-cloudflare-credentials /root/cloudflare.ini \
    -d $DOMAIN \
    -d *.$DOMAIN 
